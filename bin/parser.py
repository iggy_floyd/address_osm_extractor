#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''

    Address Extractor from OSM files

    usage: %(scriptName)s  data.pbf file.csv

'''

__epilog__='''

File:  %(scriptName)s
Author: @ Igor Marfin <iggy.floyd.de@gmail.com>
Created on Apr 23, 2015, 12:59:09 PM

'''


__author__="@ Igor Marfin <iggy.floyd.de@gmail.com>"
__date__ ="$Apr 23, 2015 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::


from os import sys, path,fsync
sys.path.append(path.dirname(path.dirname(path.abspath(__file__)))+'/lib/')
from UnicodeWriter import UnicodeWriter
from Logger import mylogging
from settings import  mylogging_threshold
mylogging.thresholdVerbosity= mylogging_threshold
from imposm.parser import OSMParser

import argparse
from argparse import RawTextHelpFormatter




# AddressExtractor
# --------------------------------------------
# The class is responsible for extracting addresses
# ::

class AddressExtractor:
    def __init__(self, csv_file,encoding="utf-8",filemode="aw"):
        self.encoding = encoding
        self.outputfile = open(csv_file,filemode)
        self.outputfile_addr = open(csv_file+'.addresses',filemode)
        self.writer = UnicodeWriter(self.outputfile,encoding=self.encoding)
        self.writer_addr = UnicodeWriter(self.outputfile_addr,encoding=self.encoding)
        #self.writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        self.num_addresses = 0

    def extract(self,tags, key):
        value = tags[key] if key in tags else ''
        try:
             value_decoded = unicode(value)
        except Exception,e:
             value_decoded=value.decode(self.encoding)
        return value_decoded
        #return value.encode('utf-8')

    def nodes(self, nodes):
        for osmid, tags, refs in nodes:
            if 'addr:city' in tags and 'addr:postcode' in tags and 'addr:street' in tags and 'addr:housenumber' in tags:
                self.num_addresses += 1

                city = self.extract(tags, 'addr:city')
                pc = self.extract(tags, 'addr:postcode')
                street = self.extract(tags, 'addr:street')
                house_number = self.extract(tags, 'addr:housenumber')
                self.writer.writerow([unicode(refs[0]), unicode(refs[1]), city, pc, street, house_number])
                self.writer_addr.writerow(['DE',street,house_number,pc,city])

    def close(self):
        self.outputfile.close()
        self.outputfile_addr.close()



# The main entrance point of the parser.py
# ----------------------------------------------------------------
#
#  .. code-block:: bash
#
#                  ~/project/bin> ./parser.py ../data.pbf ../data.csv
#
# ::




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__% {'scriptName' : sys.argv[0]},epilog=__epilog__% {'scriptName' : sys.argv[0]},add_help=True,formatter_class=RawTextHelpFormatter)
    parser.add_argument('datafilename')
    parser.add_argument('outputfilename')
    parser.add_argument('--concurrency',action="store", dest="concurrency",default=4,type=int)

    args = vars(parser.parse_args())

    # set the concurrency
    concurrency =  args['concurrency'] if  args['concurrency']>0 else 4

    # Create an AddressExtractor
    mylogging('Create an AddressExtractor')
    extractor = AddressExtractor(args['outputfilename'])


    # Create a OSMParser
    mylogging('Create an OSMParser')
    p = OSMParser(concurrency=concurrency, nodes_callback=extractor.nodes)
    mylogging('Parsing starts...')
    p.parse(args['datafilename'])
    mylogging('Parsing has ended...')
    mylogging('Found %i addresses.' % extractor.num_addresses)
    extractor.close()
