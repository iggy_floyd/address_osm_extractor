#!/usr/bin/env bash

# The parser script 
# ==============================================================
#
# :Date: Apr 23, 2015, 12:59:09 PM
# :File:   download.sh
# :Copyright: @ Igor Marfin <iggy.floyd.de@gamil.com>

# This script extracts the addresses from the pbf file 
#

# Usage
# -------------------------------
#
# ./parser.sh  data.pbf <-b=8.641805,53.008840,8.941361,53.188704>
#
# -b=... is the boundig box
#
#




# The code of the script
# -----------------------
#
# ::

osmconvert  $1 $2 >_tmp.osm
osmfilter _tmp.osm  --keep="addr:country= and addr:city= and addr:street= and addr:housenumber= and addr:postcode=" --ignore-depemdencies --drop-relations --drop-ways  > _tmp_filtered.osm
osmconvert _tmp_filtered.osm  --csv="addr:country addr:street addr:housenumber addr:postcode addr:city"  --csv-separator=";" -o=$1.csv
rm _tmp.osm _tmp_filtered.osm


