#! /usr/bin/env python
# -*- coding: utf-8 -*-

import math
import os
import sys
from LatLon import *


# degrees to radians
def deg2rad(degrees):
    return math.pi*degrees/180.0
# radians to degrees
def rad2deg(radians):
    return 180.0*radians/math.pi

# Semi-axes of WGS-84 geoidal reference
WGS84_a = 6378137.0  # Major semiaxis [m]
WGS84_b = 6356752.3  # Minor semiaxis [m]

# Earth radius at a given latitude, according to the WGS-84 ellipsoid [m]
def WGS84EarthRadius(lat):
    # http://en.wikipedia.org/wiki/Earth_radius
    An = WGS84_a*WGS84_a * math.cos(lat)
    Bn = WGS84_b*WGS84_b * math.sin(lat)
    Ad = WGS84_a * math.cos(lat)
    Bd = WGS84_b * math.sin(lat)
    return math.sqrt( (An*An + Bn*Bn)/(Ad*Ad + Bd*Bd) )

# Bounding box surrounding the point at given coordinates,
# assuming local approximation of Earth surface as a sphere
# of radius given by WGS84
def boundingBox(latitudeInDegrees, longitudeInDegrees, halfSideInKm):
    lat = deg2rad(latitudeInDegrees)
    lon = deg2rad(longitudeInDegrees)
    halfSide = 1000*halfSideInKm

    # Radius of Earth at given latitude
    radius = WGS84EarthRadius(lat)
    # Radius of the parallel at given latitude
    pradius = radius*math.cos(lat)

    latMin = lat - halfSide/radius
    latMax = lat + halfSide/radius
    lonMin = lon - halfSide/pradius
    lonMax = lon + halfSide/pradius

    return (rad2deg(latMin), rad2deg(lonMin), rad2deg(latMax), rad2deg(lonMax))


class BoundingBox(object):
    def __init__(self, *args, **kwargs):
        self.lat_min = None
        self.lon_min = None
        self.lat_max = None
        self.lon_max = None

    def __str__(self):
     return "(%f,%f,%f,%f)"%(self.lat_min,self.lon_min,self.lat_max,self.lon_max)



def get_bounding_box(latitude_in_degrees, longitude_in_degrees, half_side_in_miles):
    assert half_side_in_miles > 0
    assert latitude_in_degrees >= -180.0 and latitude_in_degrees  <= 180.0
    assert longitude_in_degrees >= -180.0 and longitude_in_degrees <= 180.0

    half_side_in_km = half_side_in_miles 
    lat = math.radians(latitude_in_degrees)
    lon = math.radians(longitude_in_degrees)

    radius  = 6371
    # Radius of the parallel at given latitude
    parallel_radius = radius*math.cos(lat)

    lat_min = lat - half_side_in_km/radius
    lat_max = lat + half_side_in_km/radius
    lon_min = lon - half_side_in_km/parallel_radius
    lon_max = lon + half_side_in_km/parallel_radius
    rad2deg = math.degrees

    box = BoundingBox()
    box.lat_min = rad2deg(lat_min)
    box.lon_min = rad2deg(lon_min)
    box.lat_max = rad2deg(lat_max)
    box.lon_max = rad2deg(lon_max)

    return (box)


def deg_to_dms(deg):
    d = int(deg)
    md = abs(deg - d) * 60
    m = int(md)
    sd = (md - m) * 60
    return [d, m, sd]




# The main entrance point
# ----------------------------------------------------------------
#
#  .. code-block:: bash
#
#                  ~/project/bin> ./parser.py ../data.pbf ../data.csv
#
# ::




if __name__ == "__main__":
    print "Bounding box obtained by Method 1"
    print
    print
    print
    print boundingBox(float(sys.argv[1]),float(sys.argv[2]),float(sys.argv[3]))
    print
    print 
    print "Bounding box obtained by Method 2"
    print
    print
    print
    print get_bounding_box(float(sys.argv[1]),float(sys.argv[2]),float(sys.argv[3]))
    print
    print
    print "Bounding box in (Degrees, Minutes and Seconds) suitable in Google Maps:  Method 1"
    print
    print
    print
    latMin,lonMin,latMax,lonMax=boundingBox(float(sys.argv[1]),float(sys.argv[2]),float(sys.argv[3]))
    print
#    print 	
#    print (deg_to_dms(latMin),deg_to_dms(lonMin),deg_to_dms(latMax),deg_to_dms(lonMax))
    print
    print 
    min_point,max_point=LatLon(latMin,lonMin),LatLon(latMax,lonMax)
    points = (min_point,max_point)
    print map(lambda x: x.to_string('d% %m% %S% %H'),points)
    print
    print	    
    print
    print "Bounding box in (Degrees, Minutes and Seconds) suitable in Google Maps:  Method 2"
    print
    print
    print
    box = get_bounding_box(float(sys.argv[1]),float(sys.argv[2]),float(sys.argv[3]))
    latMin,lonMin,latMax,lonMax=box.lat_min,box.lon_min,box.lat_max,box.lon_max
    min_point,max_point=LatLon(latMin,lonMin),LatLon(latMax,lonMax)
    points = (min_point,max_point)
    print map(lambda x: x.to_string('d% %m% %S% %H'),points)
    print
    print	    
    print
    print "Bounding box for osmfilter"
    print
    print
    print
    print "-b=%f,%f,%f,%f"%(lonMin,latMin,lonMax,latMax)


