#!/usr/bin/env bash

# The start of the service script 
# ==============================================================
#
# :Date: Apr 23, 2015, 12:59:09 PM
# :File: start_service.sh
# :Copyright: @ Igor Marfin <iggy.floyd.de@gamil.com>

# This script start the service in the Docker container
#

# Usage
# -------------------------------
#
# ./start_service.sh




# The code of the script
# -----------------------
#
# ::

trap_function(){
  echo "stopping now!"
  kill -TERM $PID
  wait $PID
  echo "sleeping@the end..."
  sleep 2
}





main() {
/usr/sbin/sshd -D &
PID=$!
}

trap 'trap_function' TERM 
main
echo "SSH DAEMON PID=$PID"
wait $PID
