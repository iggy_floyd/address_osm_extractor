reStructuredText Demonstration
******************************

Examples of Syntax Constructs

     Author: David Goodger

     Address: 123 Example Street Example, EX  Canada A1B 2C3

     Contact: <docutils-develop@lists.sourceforge.net>

     Authors: Me; Myself; I

     organization: humankind

     date: $Date: 2012-01-03 19:23:53 +0000 (Tue, 03 Jan 2012) $

     status: This is a "work in progress"

     revision: $Revision: 7302 $

     version: 1

     copyright: This document has been placed in the public domain. You
     may do with it as you wish. You may copy, modify, redistribute,
     reattribute, sell, buy, rent, lease, destroy, or improve it, quote
     it at length, excerpt, incorporate, collate, fold, staple, or
     mutilate it, or do anything else to it that your or anyone else's
     heart desires.

     field name: This is a generic bibliographic field.

     field name 2: Generic bibliographic fields may contain multiple
     body elements.

     Like this.

     Dedication: For Docutils users & co-developers.

     abstract: This document is a demonstration of the reStructuredText
     markup language, containing examples of all basic reStructuredText
     constructs and many advanced constructs.

1 Structural Elements
*********************

1.1 Section Title
=================

That's it, the text just above this line.

1.2 Transitions
===============

Here's a transition:

______________________________________________________________________


It divides the section.

2 Body Elements
***************

2.1 Paragraphs
==============

A paragraph.

2.1.1 Inline Markup
-------------------

Paragraphs contain text and may contain inline markup: _emphasis_,
*strong emphasis*, `inline literals', standalone hyperlinks
(<http://www.python.org>), external hyperlinks (Python
(http://www.python.org/) (5)), internal cross-references (*note
example: 6.), external hyperlinks with embedded URIs (Python web site
(http://www.python.org)), footnote references (manually numbered (1),
anonymous auto-numbered (3), labeled auto-numbered (2), or symbolic
(*)), citation references ([CIT2002]), substitution references
([image]), and inline hyperlink targets (see *note Targets: 8. below
for a reference back to here).  Character-level inline markup is also
possible (although exceedingly ugly!) in _re_`Structured'_Text_.

  The default role for interpreted text is Title Reference.  Here are
some explicit interpreted text roles: a PEP reference (PEP 287
(http://www.python.org/dev/peps/pep-0287)); an RFC reference (RFC 2822
(http://www.faqs.org/rfcs/rfc2822.html)); a [subscript]; a ^superscript;
and explicit roles for _standard_ *inline* `markup'.

  Let's test wrapping and whitespace significance in inline literals:
`This is an example of --inline-literal --text, --including some--
strangely--hyphenated-words.  Adjust-the-width-of-your-browser-window
to see how the text is wrapped.  -- ---- --------  Now note    the
spacing    between the    words of    this sentence    (words should
be grouped    in pairs).'

  If the `--pep-references' option was supplied, there should be a live
link to PEP 258 here.

2.2 Bullet Lists
================

   - A bullet list

        + Nested bullet list.

        + Nested item 2.

   - Item 2.

     Paragraph 2 of item 2.

        * Nested bullet list.

        * Nested item 2.

             - Third level.

             - Item 2.

        * Nested item 3.

2.3 Enumerated Lists
====================

  1. Arabic numerals.

       a. lower alpha)

            1. (lower roman)

                 A. upper alpha.

                      1. upper roman)

  2. Lists that don't start at 1:

       3. Three

       4. Four

       3. C

       4. D

       3. iii

       4. iv

  3. List items may also be auto-enumerated.

2.4 Definition Lists
====================

Term
     Definition

Term : classifier
     Definition paragraph 1.

     Definition paragraph 2.

Term
     Definition

2.5 Field Lists
===============

     what: Field lists map field names to field bodies, like database
     records.  They are often part of an extension syntax.  They are an
     unambiguous variant of RFC 2822 fields.

     how arg1 arg2: The field marker is a colon, the field name, and a
     colon.

     The field body may contain one or more body elements, indented
     relative to the field marker.

2.6 Option Lists
================

For listing command-line options:

`-a'
     command-line option "a"

`-b file'
     options can have arguments and long descriptions

`--long'
     options can be long also

`--input=file'
     long options can also have arguments

`--very-long-option'
     The description can also start on the next line.

     The description may contain multiple body elements, regardless of
     where it starts.

`-x'
`-y'
`-z'
     Multiple options are an "option group".

`-v'
`--verbose'
     Commonly-seen: short & long options.

`-1 file'
`--one=file'
`--two file'
     Multiple options with arguments.

`/V'
     DOS/VMS-style options too

  There must be at least two spaces between the option and the
description.

2.7 Literal Blocks
==================

Literal blocks are indicated with a double-colon ("::") at the end of
the preceding paragraph (over there `-->').  They can be indented:

    if literal_block:
        text = 'is left as-is'
        spaces_and_linebreaks = 'are preserved'
        markup_processing = None

Or they can be quoted without indentation:

    >> Great idea!
    >
    > Why didn't I think of that?


2.8 Line Blocks
===============

    This is a line block.  It ends with a blank line. 
        Each new line begins with a vertical bar ("|"). 
        Line breaks and initial indents are preserved. 
    Continuation lines are wrapped portions of long lines; they begin with a space in place of the vertical bar. 
        The left edge of a continuation line need not be aligned with the left edge of the text above it. 

    This is a second line block. 
     
    Blank lines are permitted internally, but they must begin with a "|". 
Take it away, Eric the Orchestra Leader!

         A one, two, a one two three four 
          
         Half a bee, philosophically, 
             must, _ipso facto_, half not be. 
         But half the bee has got to be, 
             _vis a vis_ its entity.  D'you see? 
              
         But can a bee be said to be 
             or not to be an entire bee, 
                 when half the bee is not a bee, 
                     due to some ancient injury? 
                      
         Singing... 

2.9 Block Quotes
================

Block quotes consist of indented body elements:

     My theory by A. Elk.  Brackets Miss, brackets.  This theory goes
     as follows and begins now.  All brontosauruses are thin at one
     end, much much thicker in the middle and then thin again at the
     far end.  That is my theory, it is mine, and belongs to me and I
     own it, and what it is too.

                                                        Anne Elk (Miss)

2.10 Doctest Blocks
===================

    >>> print 'Python-specific usage examples; begun with ">>>"'
    Python-specific usage examples; begun with ">>>"
    >>> print '(cut and pasted from interactive Python sessions)'
    (cut and pasted from interactive Python sessions)


2.11 Tables
===========

Here's a grid table followed by a simple table:

Header row, column 1         Header 2         Header 3       Header 4
(header rows optional)                                       
---------------------------------------------------------------------------- 
body row 1, column 1         column 2         column 3       column 4
body row 2                   Cells may span                  
                             columns.                        
body row 3                   Cells may span      - Table     
                             rows.                 cells     
                                                             
                                                 - contain   
                                                             
                                                 - body      
                                                   elements. 
body row 4                                                   
body row 5                   Cells may also                  
                             be empty: `-->'                 

Inputs              Output
------------------------------- 
A         B         A or B
False     False     False
True      False     True
False     True      True
True      True      True

2.12 Footnotes
==============

     (1) A footnote contains body elements, consistently indented by at
     least 3 spaces.

     This is the footnote's second paragraph.

     (2) Footnotes may be numbered, either manually (as in (1)) or
     automatically using a "#"-prefixed label.  This footnote has a
     label so it can be referred to from multiple places, both as a
     footnote reference ((2)) and as a hyperlink reference (*note
     label: 14.).

     (3) This footnote is numbered automatically and anonymously using a
     label of "#" only.

     (*) Footnotes may also use symbols, specified with a "*" label.
     Here's a reference to the next footnote: (†).

     (†) This footnote shows the next symbol in the sequence.

     (4) Here's an unreferenced footnote, with a reference to a
     nonexistent footnote:

2.13 Citations
==============

     (CIT2002) Citations are text-labeled footnotes. They may be
     rendered separately and differently from footnotes.

  Here's a reference to the above, [CIT2002].  citation.

2.14 Targets
============

This paragraph is pointed to by the explicit "example" target. A
reference can be found under *note Inline Markup: 5, above. *note
Inline hyperlink targets: 7. are also possible.

  Section headers are implicit targets, referred to by name. See *note
Targets: 8, which is a subsection of *note Body Elements: 3.

  Explicit external targets are interpolated into references such as
"Python (http://www.python.org/) (5)".

  Targets may be indirect and anonymous.  Thus *note this phrase: 8.
may also refer to the *note Targets: 8. section.

  Here's a hyperlink reference without a target, which generates an
error.

2.14.1 Duplicate Target Names
-----------------------------

Duplicate names in section headers or other implicit targets will
generate "info" (level-1) system messages.  Duplicate names in explicit
targets will generate "warning" (level-2) system messages.

2.14.2 Duplicate Target Names
-----------------------------

Since there are two "Duplicate Target Names" section headers, we cannot
uniquely refer to either of them by name.  If we try to (like this:
Duplicate Target Names), an error is generated.

2.15 Directives
===============

These are just a sample of the many reStructuredText Directives.  For
others, please see
<http://docutils.sourceforge.net/docs/ref/rst/directives.html>.

2.15.1 Document Parts
---------------------

An example of the "contents" directive can be seen above this section
(a local, untitled table of *note contents: 1c.) and at the beginning
of the document (a document-wide *note table of contents: 1d.).

2.15.2 Images
-------------

An image directive (also clickable - a hyperlink reference):

  [image]A figure directive:

[image]

re               Revised, revisited, based on 're' module.
Structured       Structure-enhanced text, structuredtext.
Text             Well it is, isn't it?

This paragraph is also part of the legend.

Figure: A figure is an image with a caption and/or a legend:

2.15.3 Admonitions
------------------

     Attention: Directives at large.

     Caution: Don't take any wooden nickels.

     Danger: Mad scientist at work!

     Error: Does not compute.

     Hint: It's bigger than a bread box.

     Important:
        - Wash behind your ears.

        - Clean up your room.

        - Call your mother.

        - Back up your data.

     Note: This is a note.

     Tip: 15% if the service is good.

     Warning: Strong prose may provoke extreme mental exertion.  Reader
     discretion is strongly advised.

     And, by the way...: You can make up your own admonition too.

2.15.4 Topics, Sidebars, and Rubrics
------------------------------------

Sidebar Title
.............

Optional Subtitle

  This is a sidebar.  It is for text outside the flow of the main text.

This is a rubric inside a sidebar
.................................

Sidebars often appears beside the main text with a border and
background color.

Topic Title
...........

     This is a topic.

This is a rubric
................

2.15.5 Target Footnotes
-----------------------

     (5) <http://www.python.org/>

2.15.6 Replacement Text
-----------------------

I recommend you try Python, the best language around
(http://www.python.org/) (5).

2.15.7 Compound Paragraph
-------------------------

This paragraph contains a literal block:

    Connecting... OK
    Transmitting data... OK
    Disconnecting... OK

and thus consists of a simple paragraph, a literal block, and another
simple paragraph.  Nonetheless it is semantically _one_ paragraph.

  This construct is called a _compound paragraph_ and can be produced
with the "compound" directive.

2.16 Substitution Definitions
=============================

An inline image ([image]) example:

  (Substitution definitions are not visible in the HTML source.)

2.17 Comments
=============

Here's one:

  (View the HTML source to see the comment.)

3 Error Handling
****************

Any errors caught during processing will generate system messages.

  There should be six messages in the following, auto-generated
section, "Docutils System Messages":

                                                               
                              



Local Variables:
coding: utf-8
End:
