================================================================================================================================
The address extractor
================================================================================================================================






:Author: Igor Marfin
:Contact: igor.marfin@unister.de
:Organization: private
:Date: Feb 24, 2016, 9:28:51 AM
:Status: draft
:Version: 1
:Copyright: This document has been placed in the public domain. You
            may do with it as you wish. You may copy, modify,
            redistribute, reattribute, sell, buy, rent, lease,
            destroy, or improve it, quote it at length, excerpt,
            incorporate, collate, fold, staple, or mutilate it, or do
            anything else to it that your or anyone else's heart
            desires.


|
|


.. admonition:: Dedication

    For  co-developers.



|
|


.. admonition:: Abstract

    The project is a simple system which extracts address from Open Street Maps.


|
|    

.. meta::
   :keywords: Deployment, Git-Flow
   :description lang=en: Deployment and Git-Flow



|
|




.. contents:: Table of Contents





----------------------------------------------------------------
Introduction to the topic and motivation
----------------------------------------------------------------


I want to introduce you to my motivation which forces me to develop this project.

We want to test the address validation using some mock data.
Why can't we use OSM database to extract addresses and form the sample file?





----------------------------------------------------------------
Installation of the package
----------------------------------------------------------------


.. Important::
  
    
    The project relies on the presence of the docker in the system.
    Before, proceed further, please, install it.

Simply, clone the project 

.. code-block:: bash

    git clone https://iggy_floyd@bitbucket.org/iggy_floyd/address_osm_extractor.git


and start the docker container


.. code-block:: 

    cd address_osm_extractor
    make start_container


It starts  building the docker container. After the building process has finished, it starts the container.

You can check the status or stop the container by running the commands:

.. code-block::

    make status_container
    make stop_container

-------------------------------
Get Addresses
-------------------------------

Here is a list of useful commands which can performed in the running Docker container.


Set up SSH keys for nice communication with the container
===========================================================


.. code-block::

    ssh-keygen -t rsa -b 4096 -C "mytest@example.com"
    cat ~/.ssh/id_rsa.pub | ssh -p 2239 root@localhost  'mkdir .ssh; cat  >> .ssh/authorized_keys'


Download OSM
===========================


.. code-block::

    # download a *.pbf file and save it as data.pbf
    ssh -p 2239 root@localhost "cd /project/; bin/download.sh europe/germany/bremen-latest.osm.pbf"


Extract addresses using ``parser.py``
======================================================


.. code-block::

    # get a help
    ssh -p 2239 root@localhost "cd /project/; bin/parser.py -h"

    # process obtained data.pbf
    sh -p 2239 root@localhost "cd /project/; bin/parser.py data.pbf data.csv"

    # process obtained data.pbf using 5 threads
    ssh -p 2239 root@localhost "cd /project/; bin/parser.py data.pbf data.csv --concurrency=5"

    # check a result
    ls data.csv*


Fix wrong addresses obtained from ``parser.py``
======================================================


Some addresses can have a wrong format. We have to fix them:


.. code-block::

    #find wrong records
    cat data.csv.addresses | sed -n -e  '/\".*;.*\"/p'

    # try to fix wrong records only
    cat data.csv.addresses | sed -n -e  's/\"\(.*\);.*\"/\1/p'

    # try to fix wrong records and save fixed records + non-wrong ones in data.txt
    cat data.csv.addresses | sed -e  's/\"\(.*\);.*\"/\1/g' -e '/\".*\"/d' > data.txt


Also it is useful to have shorten versions of the list of the extracted addresses


 .. code-block::

    # get a tail with 10 records
    tail -n10  data.csv.addresses    | sed -e  's/\"\(.*\);.*\"/\1/g' -e '/\".*\"/d' > data_tail10.txt

    # get a head with 10 records
    head -n10  data.csv.addresses    | sed -e  's/\"\(.*\);.*\"/\1/g' -e '/\".*\"/d' > data_head10.txt



Extract address using ``osmfilter`` and ``osmconvert``
======================================================


Also it is possible to extract address using ``osmconvert`` and ``osmfilter``.


.. code-block::

    # convert pbf to osm
    ssh -p 2239 root@localhost "cd /project/; osmconvert data.pbf >data.osm"

    # filter the OSM: keep only addresses
    ssh -p 2239 root@localhost "cd /project/; osmfilter data.osm  --keep=\"addr:country= and addr:city= and addr:street= and addr:housenumber= and addr:postcode=\" --ignore-depemdencies --drop-relations --drop-ways  > data_filtered.osm"

    # convert filtered osm to csv
    ssh -p 2239 root@localhost "cd /project/; osmconvert data_filtered.osm  --csv=\"addr:country addr:street addr:housenumber addr:postcode addr:city\"  --csv-separator=\";\" -o=data_filtered.csv"






The Bounding box
----------------------------------------------


In principle, you can limit the list of addresses using the bounding box around some geographical point.
You can visit ``maps.google.com`` and find some point in the city/country etc. I have got the center of Bremen from ``Google Maps`` as
``(53.098772 8.791583)``. Here the tuple is ``(lat,long)``. Then you can calculate the bounding box around this point within 10 km radius as follows:


.. code-block::

    ssh -p 2239 root@localhost "cd /project/; bin/_bounding_box_converter.py 53.098772 8.791583 10"


Then, using the output of the previous command, you extract addresses:


.. code-block::

    ssh -p 2239 root@localhost "cd /project/; osmconvert data.pbf -b=8.641805,53.008840,8.941361,53.188704 >data.osm"
    ssh -p 2239 root@localhost "cd /project/; osmfilter data.osm  --keep=\"addr:country= and addr:city= and addr:street= and addr:housenumber= and addr:postcode=\" --ignore-depemdencies --drop-relations --drop-ways  > data_filtered.osm"
    ssh -p 2239 root@localhost "cd /project/; osmconvert data_filtered.osm  --csv=\"addr:country addr:street addr:housenumber addr:postcode addr:city\"  --csv-separator=\";\" -o=data_filtered.csv"



Extract addresses using ``parser.sh``
======================================================


The ``parser.sh`` is a script which performs steps related to ``osmconvert`` and ``osmfilter``. You just do


.. code-block::

    # convert, filter and save to csv
    ssh -p 2239 root@localhost "cd /project/; bin/parser.sh data.pbf"
    # how many records do we have?
    cat data.pbf.csv  | wc -l

    # convert, filter and save to csv in the bounding box (1km radius)
    ssh -p 2239 root@localhost "cd /project/; bin/_bounding_box_converter.py 53.098772 8.791583 1"
    ssh -p 2239 root@localhost "cd /project/; bin/parser.sh data.pbf -b=8.776605,53.089779,8.806561,53.107765"
    # how many records do we have?
    cat data.pbf.csv  | wc -l






----------------------------------------------------------------
References
----------------------------------------------------------------



.. target-notes::



----------------------------------------------------------------
Documentation
----------------------------------------------------------------


Simply, run


.. code-block::

    make doc



-----------------------------------------
 Documentation of the source code 
-----------------------------------------

Here will be the documentation of the code from 
``source_doc/{bin,lib,daemons,scripts}/*rst`` inserted.

.. include:: source_doc/bin/download.sh.rst
.. include:: source_doc/bin/start_service.sh.rst
.. include:: source_doc/bin/parser.py.rst
.. include:: source_doc/bin/parser.sh.rst

