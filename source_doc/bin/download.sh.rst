   


The download script  
============================================================== 
 
:Date: Apr 23, 2015, 12:59:09 PM 
:File:   download.sh 
:Copyright: @ Igor Marfin <iggy.floyd.de@gamil.com>    


This script downloads the pbf file from geofabrik.de 
    


Usage 
------------------------------- 
 
./download.sh  europe/germany/bremen-latest.osm.pbf    
   
   
   


The code of the script 
----------------------- 
 
::    
   
   




   wget --output-document=/project/data.pbf http://download.geofabrik.de/"${1}"
   
