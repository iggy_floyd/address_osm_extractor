# -*- coding: utf-8 -*-
'''
	Stores the settings of the different classes
'''

#  File:   settings.py
#  Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
#  Created on Apr 23, 2015, 12:59:09 PM


__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Apr 23, 2015 12:59:09 PM$"


from os import sys, path,environ
import logging

local_db_path=path.dirname(path.dirname(path.abspath(__file__)))+"/local_db/"
log_dir_path=path.dirname(path.dirname(path.abspath(__file__)))+"/log/"
data_dir_path=path.dirname(path.dirname(path.abspath(__file__)))+"/data/"
lib_dir_path=path.dirname(path.dirname(path.abspath(__file__)))+"/lib/"
unknown_keyword='('+'_'*5 + 'UNKNOWN' + '_'*5 + ')'



# verbosity level
# suppress logging messages from werkzeug server
log_level= logging.ERROR # to see all possible levels levels, see  https://docs.python.org/2/library/logging.html#levels
#log_level= logging.NOTSET # to see all possible levels levels, see  https://docs.python.org/2/library/logging.html#levels
mylogging_threshold=7 # debug level by default
