#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import sys
import codecs
import cStringIO

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel,delimiter=';' ,encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.encoding=encoding
        self.writer = csv.writer(self.queue, dialect=dialect,delimiter=delimiter, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        #self.writer.writerow([s.encode("utf-8") for s in row])
        self.writer.writerow([s.encode(self.encoding) for s in row])

        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode(self.encoding)
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)



if __name__ == "__main__":
 import urllib
 f = open("test.csv", "aw")
 encoding="utf-8"
 #encoding="latin-1"

 writer = UnicodeWriter(f,encoding=encoding)
 writer.writerow([urllib.unquote("<C4><DF".replace("<","%").replace(">","")).decode('ISO-8859-2') ])

 #writer.writerow([ urllib.unquote("<C4><DF".replace("<","%").replace(">",""))])
 #writer.writerow([unicode(urllib.unquote("<C4><DF".replace("<","%").replace(">","")))])
 #writer.writerow([unicode(urllib.unquote("<C4><DF".replace("<","%").replace(">","")),'utf-8')])
 #writer.writerow([urllib.unquote("<C4><DF".replace("<","%").replace(">","")).decode('utf-8') ])
 f.close()

 #for i in range(3):
 # cells = ["hello".decode(encoding), "nǐ hǎo".decode(encoding), "你好".decode(encoding)]
 # writer.writerow(cells)
 #f.close()
 
