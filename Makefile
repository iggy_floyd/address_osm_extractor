# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project



DESCRIPTION = "Address Extractor"
IMAGE_NAME = address_osm_extractor
SSH_DOCKER_PORT=2239

$(info )
$(info )
$(info )
$(info )
$(info )
$(info *******************************************)
$(info You are working with $(DESCRIPTION)...)
$(info *******************************************)
$(info )
$(info )
$(info )
$(info )
$(info )


all:  doc


doc: README.rst
	$(info Building documention for  $(DESCRIPTION)...)
	$(info )
	-@mkdir doc 2>/dev/null
	-@cd source_doc/; ./create_documentation.sh `ls ../bin/*.py` `ls ../lib/*.py` `ls ../bin/*.sh` `ls ../scripts/*.sh` `ls ../scripts/*.py`
	-@rst-tool/create_docs.sh README.rst `basename $(PWD)` $(DESCRIPTION); mv README.html doc;
	-@ssh -p 2239 root@localhost "cd /project/; pandoc --from=rst --to=markdown  --output=README.md README.rst"	


build_image:
	$(info Start building the image $(IMAGE_NAME) ...)
	$(info )
	-@sudo docker build -t $(IMAGE_NAME) .



ifneq ($(shell sudo docker ps -a | grep $(IMAGE_NAME) | grep -o "^\w*\b" ),)
start_container:
	$(info Starting the container $(IMAGE_NAME)...)
	$(info )
	-@sudo docker start $(IMAGE_NAME)
else 
ifneq ($(shell sudo docker images | grep $(IMAGE_NAME) | grep -o "^\w*\b" ),)
start_container:
	$(info Starting the container $(IMAGE_NAME) at the first time...)
	$(info )
	-@sudo docker run -d -p $(SSH_DOCKER_PORT):22  -v `pwd`/:/project/  --name=$(IMAGE_NAME) $(IMAGE_NAME)
else 
start_container: build_image
        $(info Starting the container $(IMAGE_NAME) at the first time...)
	$(info )
        -@sudo docker run -d -p $(SSH_DOCKER_PORT):22  -v `pwd`/:/project/  --name=$(IMAGE_NAME) $(IMAGE_NAME)
endif
endif


ifneq ($(shell sudo docker ps -a | grep $(IMAGE_NAME) | grep -o "^\w*\b" ),)
stop_container:
	$(info Stopping the container $(IMAGE_NAME)...)
	$(info )
	-@sudo docker stop $(IMAGE_NAME)
endif


restart_container: stop_container start_container

status_container: 
	$(info Status of the container $(IMAGE_NAME)...)
	$(info )
	-@sudo docker ps -a  | grep  -e $(IMAGE_NAME) -e STATUS


# to clean all temporary stuff
clean:  
	$(info Removing temporary files of $(DESCRIPTION)...)
	$(info )
	-@rm -r doc 
	-@rm README

.PHONY: clean all doc build_image stop_container start_container restart_container status_container
