FROM rastasheep/ubuntu-sshd:14.04

MAINTAINER Igor Marfin <iggy.floyd.de@gmail.com>

# environment of the container
ENV TERM xterm

RUN apt-get update

# Install basic software
RUN apt-get -y install wget git-core ca-certificates curl nano

# Change locale to en_US.UTF-8
RUN locale-gen en_US.UTF-8  
RUN dpkg-reconfigure locales
RUN update-locale LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_ALL=en_US.UTF-8 
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8 

# Install Python 
RUN apt-get update; apt-get install -y python2.7 python2.7-dev

# Do following install tasks in /tmp 
WORKDIR /tmp



# Install latest pip 
RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python2.7 get-pip.py

# Install stuff needed by Python 
RUN apt-get update; apt-get install -y build-essential pkg-config libpng-dev libfreetype6-dev
RUN apt-get install -y libpng-dev libfreetype6 
RUN apt-get install -y libatlas-base-dev libatlas-dev
RUN apt-get -y install libfftw3-dev libsuitesparse-dev
RUN apt-get -y install liblapack-dev gfortran 



# Install python modules
RUN mkdir -p /project/
ADD ./provision/ /project/provision
RUN pip install -r /project/provision/requirements_python

# Install pandoc
RUN wget https://github.com/jgm/pandoc/releases/download/1.16.0.2/pandoc-1.16.0.2-1-amd64.deb
RUN dpkg -i pandoc-1.16.0.2-1-amd64.deb


# Install OSMfilter and OSMConverter
RUN wget -O - http://m.m.i24.cc/osmfilter.c |cc -x c - -O3 -o /usr/bin/osmfilter
RUN wget -O - http://m.m.i24.cc/osmconvert.c | cc -x c - -lz -O3 -o /usr/bin/osmconvert

# Install IMPOSM.PARSER
RUN apt-get -y install  protobuf-compiler libprotobuf-dev
RUN pip install imposm.parser

# Adopt .bashrc
RUN echo "cd /project/" >> ~/.bashrc 

#mount volumes
RUN mkdir -p /project/bin
VOLUME ["/project/"] 


# Exposing ports
EXPOSE 22


# WORKING DIR 
WORKDIR /project/

# Starting services		
CMD ["bin/start_service.sh"]





# RUN ME

## 1) build the docker
## sudo docker build -t address_osm_extractor .

## 2.1) run the container interactively (/bin/bash) 

## @ working server
## sudo docker run -t --rm -i -p 8098:80 -p 2238:22  -v `pwd`/:/project/  --name=address_osm_extractor address_osm_extractor /bin/bash
 

## 2.2.1) start bin/start_service.sh in  the container and dettached the container
## @ working server
## sudo docker run -d -p 8098:80 -p 2238:22  -v `pwd`/:/project/  --name=address_osm_extractor address_osm_extractor


## 2.2.2) to 'get in' the container
## sudo docker exec -i -t address_osm_extractor  /bin/bash
## 2.2.3) or
## ssh -p 2238 root@localhost


## 3) Stop the container
## sudo docker stop address_osm_extractor

## 4) check processes
## sudo docker exec address_osm_extractor ps -ef
## get logs
## sudo docker logs address_osm_extractor
## get the exit status
## sudo docker inspect -f '{{.State.ExitCode}}' address_osm_extractor

## 5)How to control docker system from the docker container?
## #get host ip: 10.0.2.15
## ip addr
## login to container
## ssh -p 2232 root@localhost
## add www-data public key to the vagrant 'user'@host
## root@ff236b4cce1a:~ cat /home/vagrant/project/provision/id_rsa.pub >> vagrant@DeploymentService:~/.ssh/authorized_keys
## test: ls command
## sudo -Hu www-data ssh vagrant@10.0.2.15 ls
## control docker 
## sudo -Hu www-data ssh vagrant@10.0.2.15 sudo docker ps -a



